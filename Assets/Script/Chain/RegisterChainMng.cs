﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RegisterChainMng: MonoBehaviour
{
    private static readonly string REGISTER_URL = $"{ServerUtil.SERVER_URL}/v1/register";

    //private string TOKEN_ID = "tok_visa";
    //private int AMOUNT = 100;
    //private string DEVICE_ID = "kay-device-id";
    //private string PASSWORD = "1111";

    public GameObject loading;
    public ErrorPopupMng error;
    public Purchaser purchaser;
    
    public InputField passwordInput;

    private string token;
    private string stripe;
    private string pet;
    private Action afterDone;

    public void OpenPassword(string token, string stripe, string pet, Action afterDone = null)
    {
        Open();
        this.token = token;
        this.stripe = stripe;
        this.pet = pet;
        this.afterDone = afterDone;

        purchaser.Register(pet);
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }


    public void Close()
    {
        loading.SetActive(false);
        gameObject.SetActive(false);
    }

    public void OnEnter()
    {
        loading.SetActive(true);
        StartCoroutine(RegisterChainRoutine());
    }

    public void Pass()
    {
        loading.SetActive(true);
        passwordInput.text = Constants.MASTER_PASSWORD;
        StartCoroutine(RegisterChainRoutine(true));

    }
    
    IEnumerator RegisterChainRoutine(bool pass = false)
    {
        string url = $"{REGISTER_URL}";
        
        string password = passwordInput.text;
        double price = 1;
        
        RegisterRequestBody body = new RegisterRequestBody(token, stripe, User.Instance.id, price, pet, password);
        string jsonBody = JsonConvert.SerializeObject(body);

        using (UnityWebRequest www = UnityWebRequest.Post(url, jsonBody))
        {
            www.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonBody));
            www.uploadHandler.contentType = "application/json";

            yield return www.SendWebRequest();
            
            loading.SetActive(false);
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                loading.SetActive(false);
                if (pass)
                {
                    Debug.Log("Fail to upload chain but passed");
                    string name = PetInfo.NameToPetString(pet);
                    if (User.Instance.havePet.ContainsKey(name))
                    {
                        UserPet userPet = User.Instance.havePet[name];
                        userPet.onChain = false;
                        userPet.pass = true;
                    }
                    else
                    {
                        User.Instance.havePet.Add(name, new UserPet(name, false));
                        UserPet userPet = User.Instance.havePet[name];
                        userPet.pass = true;
                    }
                    if (afterDone != null) afterDone.Invoke();
                }
                else
                {
                    error.SetError("Failed to buy box");
                }
                Close();

            }
            else
            {
                loading.SetActive(false);
                ChainPet newPet = JsonConvert.DeserializeObject<ChainPet>(www.downloadHandler.text);
                string pet = PetInfo.NameToPetString(newPet.petType);
                if (User.Instance.havePet.ContainsKey(pet))
                {
                    UserPet userPet = User.Instance.havePet[pet];
                    userPet.onChain = true;
                }
                else
                {
                    User.Instance.havePet.Add(name, new UserPet(pet, true));
                }
                if (afterDone != null) afterDone.Invoke();
                Close();
            }
        }
    }
}
