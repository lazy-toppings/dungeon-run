using System;
using UnityEngine;

public class NotSignedInPopupMng: MonoBehaviour
{
    public StripeTokenMng stripeTokenMng;

    public void Open()
    {
        gameObject.SetActive(true);
    }
    
    public bool IsChainUser()
    {
        return User.Instance.id != null;
    }

    public void SignUp()
    {
        Close();
        stripeTokenMng.Open("signup");
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}