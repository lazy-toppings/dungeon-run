using System;
using UnityEngine;
using UnityEngine.UI;

public class ErrorPopupMng: MonoBehaviour
{
    public void SetError(string msg)
    {
        transform.Find("ErrorText").GetComponent<Text>().text = $"{ msg }";
        Open();
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}