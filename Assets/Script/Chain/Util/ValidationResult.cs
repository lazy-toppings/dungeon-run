﻿using System;

public class ValidationResult
{
    private bool result;
    private string msg;

    public ValidationResult(bool result, string msg = "")
    {
        this.result = result;
        this.msg = msg;
    }

    public bool Result => result;
    public string Msg => msg;
}
