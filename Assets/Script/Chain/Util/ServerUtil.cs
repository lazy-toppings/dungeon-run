public class ServerUtil
{
    //public static readonly string SERVER_URL = "http://localhost:8080";
    public static readonly string SERVER_URL = "http://ec2-54-180-189-196.ap-northeast-2.compute.amazonaws.com:8080";
    public static readonly string TYPE = "klaytn";
    public static readonly string PREFIX = TYPE;
    public static readonly string GAMERECIPE_URL = $"https://{PREFIX}.gamerecipe.io/dev/v1";
}