﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SignUpMng: MonoBehaviour
{
    private static readonly string SIGNUP_URL = $"{ServerUtil.GAMERECIPE_URL}/accounts";

    //private string TOKEN_ID = "tok_visa";
    //private int AMOUNT = 100;
    //private string DEVICE_ID = "kay-device-id";
    //private string PASSWORD = "1111";

    public GameObject loading;
    public ErrorPopupMng error;

    public Dropdown typeDropdown;
    public InputField stakeNet;
    public InputField stakeCpu;
    public InputField ram;
    public InputField price;
    public InputField passwordInput;
    public InputField passwordCheckInput;

    private string token;
    private Dictionary<string, AccountTypeListResponse.AccountType> accountTypes;

    private Action afterDone;

    public void OpenSignUp(StripeTokenResponse stripeTokenResponse, Action afterDone)
    {
        Open();
        this.afterDone = afterDone;
        token = stripeTokenResponse.Id;
        loading.SetActive(true);
        FindObjectOfType<AccountTypeListMng>().GetAccountTypeList(response =>
        {
            loading.SetActive(false);
            typeDropdown.options.Clear();
            accountTypes = response.Types;
            var keyList = accountTypes.Keys.ToList();
            Debug.Log(string.Join(", ", keyList));
            typeDropdown.AddOptions(keyList);
            typeDropdown.onValueChanged.RemoveAllListeners();
            typeDropdown.onValueChanged.AddListener(delegate { DropdownValueChanged(typeDropdown); });
            DropdownValueChanged(typeDropdown);
            gameObject.SetActive(true);
        }, errorResponse =>
        {
            error.SetError(errorResponse);
            loading.SetActive(false);
            Debug.LogError($"Error : #{errorResponse}");
            Debug.LogError("failed to load account type list");
        });
    }

    public void Open()
    {
    }

    public void Close()
    {
        loading.SetActive(false);
        gameObject.SetActive(false);
    }

    public void OnSignUp()
    {
        loading.SetActive(true);
        ValidationResult validationResult = Validate();
        if (!validationResult.Result)
        {
            error.SetError(validationResult.Msg);
            loading.SetActive(false);
            Debug.LogError(validationResult.Msg);
            return;
        }

        var type = typeDropdown.options[typeDropdown.value].text;
        var password = passwordInput.text;     
        SignupRequestBody requestBody = new SignupRequestBody(token, type, password);
        string jsonData = JsonConvert.SerializeObject(requestBody);
        StartCoroutine(SendPost(SIGNUP_URL, jsonData, SuccessSignUp));
    }

    private IEnumerator SendPost(string uri, string jsonData, System.Action<SignUpResponse> callback)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(uri, jsonData))
        { 
            www.SetRequestHeader("Authorization", "Bearer " + FindObjectOfType<AuthTokenMng>().AuthToken);
            www.uploadHandler.contentType = "application/json";
            www.uploadHandler = (UploadHandler)new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonData));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                try
                {
                    SignUpErrorResponse errorResponse = JsonConvert.DeserializeObject<SignUpErrorResponse>(jsonResult);
                    string address = errorResponse.Detail.Address;
                    if (!string.IsNullOrEmpty(address))
                    {
                        callback(new SignUpResponse(address));
                    }
                    else
                    {
                        throw new Exception("Invalid account");
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                    ErrorSignUp(jsonResult);
                    loading.SetActive(false);
                }
            }
            else
            {
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                callback(JsonConvert.DeserializeObject<SignUpResponse>(jsonResult));
            }
        }
    }

    void SuccessSignUp(SignUpResponse response)
    {
        Debug.Log("Account : " + response.Account);
        User.Instance.id = response.Account;
        loading.SetActive(false);
        afterDone?.Invoke();
        Close();
    }

    void ErrorSignUp(string jsonResult)
    {
        error.SetError(jsonResult);
        loading.SetActive(false);
        Debug.LogError($"Error : {jsonResult}");
    }

    private void DropdownValueChanged(Dropdown changed)
    {
        SetInfo(changed.options[changed.value].text);
    }

    private void SetInfo(string type)
    {
        AccountTypeListResponse.AccountType accountType = accountTypes[type];
        stakeNet.text = accountType.StakeNet;
        stakeCpu.text = accountType.StakeCpu;
        ram.text = accountType.BuyRam;
        price.text = accountType.Price;
    }

    private ValidationResult Validate()
    {
        return ValidatePassword();
    }

    private ValidationResult ValidatePassword()
    {
        if (!passwordInput.text.Equals(passwordCheckInput.text))
        {
            return new ValidationResult(false, "password and password check are different");
        }
        // Minimum eight characters, at least one letter, one number and one special character
        const string pattern = @"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$";
        if (!Regex.Match(passwordInput.text, pattern).Success)
        {
            return new ValidationResult(false, "minimum eight characters, at least one letter, one number and one special character");
        }
        return new ValidationResult(true);
    }
}
