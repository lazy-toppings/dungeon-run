﻿using System;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class SignUpResponse
{
    [JsonProperty] private string account;

    public SignUpResponse(string account)
    {
        this.account = account;
    }

    public string Account => account;
}
