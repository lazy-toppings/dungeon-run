﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class AccountTypeListResponse
{
    [JsonProperty] private Dictionary<string, AccountType> types;
    [JsonProperty] private string updatedAt;

    public AccountTypeListResponse(Dictionary<string, AccountType> types, string updatedAt)
    {
        this.types = types;
        this.updatedAt = updatedAt;
    }

    public Dictionary<string, AccountType> Types => types;
    public string UpdatedAt => updatedAt;

    [Serializable]
    public class AccountType
    {
        [JsonProperty(PropertyName = "stake-net")] private string stakeNet;
        [JsonProperty(PropertyName = "stake-cpu")] private string stakeCpu;
        [JsonProperty(PropertyName = "buy-ram")] private string buyRam;
        [JsonProperty] private string price;

        public AccountType(string stakeNet, string stakeCpu, string buyRam, string price)
        {
            this.stakeNet = stakeNet;
            this.stakeCpu = stakeCpu;
            this.buyRam = buyRam;
            this.price = price;
        }

        public string StakeNet => stakeNet;
        public string StakeCpu => stakeCpu;
        public string BuyRam => buyRam;
        public string Price => price;
    }

}

