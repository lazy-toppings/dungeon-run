﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class AssetListResponse
{
    [JsonProperty] private List<Asset> results;

    public AssetListResponse(List<Asset> results)
    {
        this.results = results;
    }

    public List<Asset> Results => results;
}

[Serializable]
public class Asset
{
    [JsonProperty] private string id;
    [JsonProperty] private string tokenId;
    [JsonProperty] private string name;
    [JsonProperty] private string description;
    [JsonProperty] private string image;
    [JsonProperty] private Dictionary<string, Attribute> attributes;
    [JsonProperty] private Dictionary<string, Attribute> extra;
    [JsonProperty] private DateTime created;

    public Asset(string id, string tokenId, string name, string description, string image, Dictionary<string, Attribute> attributes, Dictionary<string, Attribute> extra, DateTime created)
    {
        this.id = id;
        this.tokenId = tokenId;
        this.name = name;
        this.description = description;
        this.image = image;
        this.attributes = attributes;
        this.extra = extra;
        this.created = created;
    }

    public string Id => id;
    public string TokenId => tokenId;
    public string Name => name;
    public string Description => description;
    public string Image => image;
    public Dictionary<string, Attribute> Attributes => attributes;
    public Dictionary<string, Attribute> Extra => extra;
    public DateTime Created => created;
}

[Serializable]
public class Detail
{
    [JsonProperty] private string imageSmall;
    [JsonProperty] private string imageLarge;
    [JsonProperty] private string lang;
    [JsonProperty] private string description;
    [JsonProperty] private Details details;

    public Detail(string imageSmall, string imageLarge, string lang, string description, Details details)
    {
        this.imageSmall = imageSmall;
        this.imageLarge = imageLarge;
        this.lang = lang;
        this.description = description;
        this.details = details;
    }

    public string ImageSmall => imageSmall;
    public string ImageLarge => imageLarge;
    public string Lang => lang;
    public string Description => description;
    public Details Details => details;
}

[Serializable]
public class Details
{
    [JsonProperty] private string name;
    [JsonProperty] private string description;
    [JsonProperty] private string creator;
    [JsonProperty] private string price;
    [JsonProperty] private Dictionary<string, Attribute> attributes;

    public Details(string name, string description, string creator, string price, Dictionary<string, Attribute> attributes)
    {
        this.name = name;
        this.description = description;
        this.creator = creator;
        this.price = price;
        this.attributes = attributes;
    }

    public string Name => name;
    public string Description1 => description;
    public string Creator => creator;
    public string Price => price;
    public Dictionary<string, Attribute> Attributes => attributes;
}

[Serializable]
public class Attribute
{
    [JsonProperty] private bool mutable;
    [JsonProperty] private float value;

    public Attribute(bool mutable, float value)
    {
        this.mutable = mutable;
        this.value = value;
    }

    public bool Mutable => mutable;
    public float Value => value;
}
