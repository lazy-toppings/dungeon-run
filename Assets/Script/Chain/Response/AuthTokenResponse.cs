using System;
using Newtonsoft.Json;

[Serializable]
public class AuthTokenResponse
{
    [JsonProperty] private string token;
    [JsonProperty] private string stripe;

    public AuthTokenResponse(string token, string stripe)
    {
        this.token = token;
        this.stripe = stripe;
    }

    public string Token => token;
    public string Stripe => stripe;
}