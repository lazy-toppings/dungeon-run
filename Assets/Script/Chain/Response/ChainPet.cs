using System;

[Serializable]
public class ChainPet
{
    public string assetId;
    public string petType;

    public ChainPet(string assetId, string petType)
    {
        this.assetId = assetId;
        this.petType = petType;
    }
}