﻿using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class StripeTokenMng : MonoBehaviour
{
    public GameObject loading;
    public ErrorPopupMng error;

    public InputField cardNumber1;
    public InputField cardNumber2;
    public InputField cardNumber3;
    public InputField cardNumber4;
    public InputField monthInput;
    public InputField yearInput;
    public InputField cvcInput;

    public Text cardTitle;
    
    private static readonly string CREATE_TOKEN_URL = "https://api.stripe.com/v1/tokens";
    private string authToken;
    private string stripeKey;

    private string type;

    public Action afterDone;

    public void Open(string type, Action afterDone = null)
    {
        this.type = type;
        if (type == "signup") cardTitle.text = "Register Card";
        else cardTitle.text = "Payment";
        loading.SetActive(true);
        this.afterDone = afterDone;
        AuthTokenMng authTokenMng = FindObjectOfType<AuthTokenMng>();
        authTokenMng.GetAuthToken(response =>
        {
            loading.SetActive(false);
            Debug.Log($"{response.Token}, {response.Stripe}");
            stripeKey = response.Stripe;
            authToken = response.Token;    
            // TODO pass form
            // gameObject.SetActive(true);
            PassStripe();

        }, errorResponse =>
        {
            loading.SetActive(false);
            Debug.LogError($"Error : {errorResponse}");
            error.SetError(errorResponse);
        });
    }
    
    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void OnSubmit()
    {
        loading.SetActive(true);
        ValidationResult validationResult = Validate();
        if (!validationResult.Result)
        {
            error.SetError(validationResult.Msg);
            loading.SetActive(false);
            Debug.LogError(validationResult.Msg);
            return;
        }
        var cardNumber = cardNumber1.text + cardNumber2.text + cardNumber3.text + cardNumber4.text;
        var expMonth = int.Parse(monthInput.text);
        var expYear = int.Parse(yearInput.text);
        var cvc = int.Parse(cvcInput.text);
        
        StripeTokenRequestBody requestBody = new StripeTokenRequestBody(cardNumber, expMonth, expYear, cvc);
        StartCoroutine(SendCardForm(CREATE_TOKEN_URL, requestBody, stripeKey, GetTokenInfo));
    }
    
    private IEnumerator SendCardForm(string uri, StripeTokenRequestBody requestBody, string secretKey, System.Action<StripeTokenResponse> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("card[number]", requestBody.CardNumber);
        form.AddField("card[exp_month]", requestBody.ExpMonth);
        form.AddField("card[exp_year]", requestBody.ExpYear);
        form.AddField("card[cvc]", requestBody.Cvc);

        using (UnityWebRequest www = UnityWebRequest.Post(uri, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + secretKey);

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogError(www.error);
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                StripeError(JsonConvert.DeserializeObject<StripeErrorResponse>(jsonResult));
                loading.SetActive(false);
            }
            else
            {
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                callback(JsonConvert.DeserializeObject<StripeTokenResponse>(jsonResult));
            }
        }
    }

    private void StripeError(StripeErrorResponse response)
    {
        error.SetError(response.Error.Message);
        Debug.LogError(response.Error.Message);
        loading.SetActive(false);
    }

    private void GetTokenInfo(StripeTokenResponse stripeTokenResponse)
    {
        loading.SetActive(false);
        Debug.Log("Token ID : " + stripeTokenResponse.Id);
        Close();

        if (type == "signup")
        {
            if (ServerUtil.TYPE == "eos")
            {
                var signUpMng = gameObject.transform.parent.Find("SignUp").gameObject.GetComponent<SignUpMng>();
                signUpMng.OpenSignUp(stripeTokenResponse, afterDone);
            }
            else
            {
                
                var signUpMng = gameObject.transform.parent.Find("SimpleSignUp").gameObject.GetComponent<SimpleSignUpMng>();
                signUpMng.OpenSignUp(stripeTokenResponse, afterDone);
            }
        }
        else
        {
            var registerChainMng = gameObject.transform.parent.Find("CheckPassword").gameObject.GetComponent<RegisterChainMng>();
            registerChainMng.OpenPassword(authToken, stripeTokenResponse.Id, PetInfo.PetName(type), afterDone);
        }
    }
    
    private void PassStripe()
    {
        Close();
        GetTokenInfo(new StripeTokenResponse(Constants.MASTER_STRIPE_KEY));
    }

    private ValidationResult Validate()
    {
        ValidationResult cardNumberResult = ValidateCardNumber();
        if (!cardNumberResult.Result) return cardNumberResult;
        ValidationResult dateResult = ValidateExpirationDate();
        return dateResult.Result ? ValidateCvc() : dateResult;
    }

    private ValidationResult ValidateCardNumber()
    {
        const string pattern = @"\d{4}";
        if (Regex.Match(cardNumber1.text, pattern).Success &&
            Regex.Match(cardNumber2.text, pattern).Success &&
            Regex.Match(cardNumber3.text, pattern).Success &&
            Regex.Match(cardNumber4.text, pattern).Success)
        {
            return new ValidationResult(true);
        }
        return new ValidationResult(false, "invalid card number");
    }

    private ValidationResult ValidateExpirationDate()
    {
        if (int.TryParse(monthInput.text, out var expMonth) &&
            1 <= expMonth && expMonth <= 12 &&
            int.TryParse(yearInput.text, out var expYear) &&
            expYear >= 2019)
        {
            return new ValidationResult(true);
        }
        return new ValidationResult(false, "invalid expiration date");
    }

    private ValidationResult ValidateCvc()
    {
        bool result = int.TryParse(cvcInput.text, out _);
        return new ValidationResult(result, result ? "" : "invalid cvc");
    }
}
