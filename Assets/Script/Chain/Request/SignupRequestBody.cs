﻿using System;
using Newtonsoft.Json;

[Serializable]
public class SignupRequestBody
{
    [JsonProperty] private string token;
    [JsonProperty] private string type;
    [JsonProperty] private string password;

    public SignupRequestBody(string token, string type, string password)
    {
        this.token = token;
        this.type = type;
        this.password = password;
    }

    public string Token => token;
    public string Type => type;
    public string Password => password;
}
