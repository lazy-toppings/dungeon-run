using System;
using Newtonsoft.Json;

[Serializable]
public class RegisterRequestBody
{
    [JsonProperty] private string token;
    [JsonProperty] private string stripeToken;
    [JsonProperty] private string account;
    [JsonProperty] private double price;
    [JsonProperty] private string pet;
    [JsonProperty] private string password;

    public RegisterRequestBody(string token, string stripeToken, string account, double price, string pet, string password)
    {
        this.token = token;
        this.stripeToken = stripeToken;
        this.account = account;
        this.price = price;
        this.pet = pet;
        this.password = password;
    }
}