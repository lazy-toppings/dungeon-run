﻿﻿using System.Collections;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class AssetListMng : MonoBehaviour
{
    private static readonly string LIST_ASSET_URL = $"{ServerUtil.GAMERECIPE_URL}/accounts/{{0}}/assets?detail=true";

    public void GetAssetList(string token, string account, System.Action<AssetListResponse> callback, System.Action<string> errorCallback)
    {
        StartCoroutine(ListAsset(string.Format(LIST_ASSET_URL, account), token, callback, errorCallback));
    }

    private IEnumerator ListAsset(string uri, string authToken, System.Action<AssetListResponse> callback, System.Action<string> errorCallback)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(uri))
        {
            www.SetRequestHeader("Authorization", "Bearer " + authToken);
            www.SetRequestHeader("Network", "baobab");
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                errorCallback(jsonResult);
            }
            else
            {
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                callback(JsonConvert.DeserializeObject<AssetListResponse>(jsonResult));
            }
        }
    }
}
