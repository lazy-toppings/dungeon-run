﻿using System;
using System.Collections;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class AccountTypeListMng : MonoBehaviour
{
    public static readonly string ACCOUNT_TYPE_LIST_URL = $"{ServerUtil.GAMERECIPE_URL}/accounts/types";

    public void GetAccountTypeList(Action<AccountTypeListResponse> callback, Action<string> errorCallback)
    {
        StartCoroutine(RequestAccountTypeList(ACCOUNT_TYPE_LIST_URL, callback, errorCallback));
    }

    private IEnumerator RequestAccountTypeList(string uri, Action<AccountTypeListResponse> callback,
        Action<string> errorCallback)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(uri))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogError(www.error);
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                errorCallback(jsonResult);
            }
            else
            {
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                callback(JsonConvert.DeserializeObject<AccountTypeListResponse>(jsonResult));
            }
        }
    }
}
