﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System;

public class BulletInfo : Singleton<BulletInfo>
{
    private readonly string filepath = "bullet.json";
    
    public Dictionary<string, List<string>> petBullet; // [pet] = available bullets
    public Dictionary<string, Bullet> bullets;
    public float speed;

    public BulletInfo()
    {
        Load();
    }

    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        var bytes = File.ReadAllBytes(path);
        //        var jsonStr = Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
        var jsonStr = Encoding.UTF8.GetString(bytes);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            //var jsonStr = Encoding.UTF8.GetString(reader.bytes, 3, reader.bytes.Length - 3);
        var jsonStr = Encoding.UTF8.GetString(reader.bytes);
#endif
        //Debug.Log(jsonStr);
        var jsonConfig = JsonConvert.DeserializeObject<BulletInfoWrapper>(jsonStr);
        petBullet = jsonConfig.petBullet;
        bullets = jsonConfig.bullets;
        speed = jsonConfig.speed;
    }

    [Serializable]
    public class BulletInfoWrapper
    {
        public Dictionary<string, List<string>> petBullet; // [pet] = available bullets
        public Dictionary<string, Bullet> bullets;
        public float speed;
    }
}