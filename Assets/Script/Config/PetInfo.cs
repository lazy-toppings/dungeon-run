﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

public class PetInfo : Singleton<PetInfo>
{
    private readonly string filepath = "pet.json";

    public Dictionary<string, Pet> pets;

    public List<string> possible;

    public float jumpTime;
    public float keepTime;
    public float fallTime;

    public float jumpHeight;
    
    public PetInfo()
    {
        Load();
    }

    public static string NameToPetString(string name)
    {
        switch (name)
        {
            case "Slime": return "0";
            case "Jaglime": return "1";
            case "Skygon": return "2";
            case "Firegon": return "3";
            case "Cablin": return "5";
            case "Pablin": return "6";
            case "Loopa": return "4";
            case "Filout": return "7";
            case "Gouse": return "8";
            case "Nouse": return "9";
            case "Rabitan": return "10";
            case "Pabitan": return "11";
            case "Devlic": return "12";
            case "Sprouce": return "13";
            case "Prouce": return "14";
            case "Ploon": return "15";
            case "Ploony": return "16";
            case "Squirim": return "17";
            case "Sleequir": return "18";
            case "Snupack": return "19";
            default: return null;
        }
    }
    
    public static string PetName(string pet)
    {
        switch (pet)
        {
            case "0": return "Slime";
            case "1": return "Jaglime";
            case "2": return "Skygon";
            case "3": return "Firegon";
            case "5": return "Cablin";
            case "6": return "Pablin";
            case "4": return "Loopa";
            case "7": return "Filout";
            case "8": return "Gouse";
            case "9": return "Nouse";
            case "10": return "Rabitan";
            case "11": return "Pabitan";
            case "12": return "Devlic";
            case "13": return "Sprouce";
            case "14": return "Prouce";
            case "15": return "Ploon";
            case "16": return "Ploony";
            case "17": return "Squirim";
            case "18": return "Sleequir";
            case "19": return "Snupack";
            default: return null;
        }
    }
    
    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        
        var bytes = File.ReadAllBytes(path);
        //        var jsonStr = Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
        var jsonStr = Encoding.UTF8.GetString(bytes);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            //var jsonStr = Encoding.UTF8.GetString(reader.bytes, 3, reader.bytes.Length - 3);
        var jsonStr = Encoding.UTF8.GetString(reader.bytes);
#endif
        // Debug.Log(jsonStr);
        var jsonConfig = JsonConvert.DeserializeObject<PetInfoWrapper>(jsonStr);
        pets = jsonConfig.pets;
        possible = jsonConfig.possible;
        jumpTime = jsonConfig.jumpTime;
        keepTime = jsonConfig.keepTime;
        fallTime = jsonConfig.fallTime;
        jumpHeight = jsonConfig.jumpHeight;
    }

    [Serializable]
    public class PetInfoWrapper
    {
        public List<string> possible;

        public float jumpTime;
        public float keepTime;
        public float fallTime;

        public float jumpHeight;
        public Dictionary<string, Pet> pets;
    }
}