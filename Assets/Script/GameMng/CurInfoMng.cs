﻿using UnityEngine;
using System.Collections;
using UnityEngine.U2D;

public class CurInfoMng : MonoBehaviour
{
    public SpriteAtlas bulletAtlas;

    private readonly string standPetPath = "Prefabs/StandPet/";

    private SpriteRenderer bullet;
    private Transform pet;

    void Start()
    {
        bullet = transform.Find("Bullet").Find("Img").GetComponent<SpriteRenderer>();
        pet = transform.Find("Pet");
        SetImgs();
    }

   public void SetImgs()
    {
        bullet.sprite = bulletAtlas.GetSprite($"{User.Instance.cur.pet}_{User.Instance.cur.bullet}");

        if (pet.childCount != 0)
        {
            if (pet.GetChild(pet.childCount - 1).name == User.Instance.cur.pet) return;
            else pet.GetChild(pet.childCount - 1).gameObject.SetActive(false);
        }
        GameObject g = Instantiate(Resources.Load($"{standPetPath}{User.Instance.cur.pet}")) as GameObject;
        g.transform.SetParent(pet);
        g.name = User.Instance.cur.pet;
        g.transform.localPosition = new Vector2(0, 0);
    }
}
