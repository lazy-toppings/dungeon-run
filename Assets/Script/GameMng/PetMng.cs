﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PetMng : MonoBehaviour
{
    public PlayMng playMng;

    public Slider hpSlider;
    public Text scoreText;

    private Animator animator;
    public Transform pet;
    private Movement movement;
    private Vector2 defaultPos;

    private bool isJumping;
    private bool isDoubleJumping;

    private bool isPowerMode;
    private bool isPause;

    private float hp;
    private int score;
    
    private int petLevel;
    private Pet petInfo;
    private float monsterDamage;
    
    public AudioSource attackSound;
    public AudioSource getSound;

    private Coroutine afterJumpRoutine;

    void Start()
    {
        defaultPos = transform.position;

        animator = GetComponent<Animator>();
        pet = transform.GetChild(0);
        movement = pet.GetComponent<Movement>();
        
        petLevel = User.Instance.havePet[User.Instance.cur.pet].level;
        petInfo = PetInfo.Instance.pets[User.Instance.cur.pet];
        monsterDamage = Monster.Instance.damage * petInfo.damageControl[petLevel];

        InitValues();
    }

    private void InitValues()
    {
        hpSlider.maxValue = petInfo.hp[petLevel];

        hp = petInfo.hp[petLevel];
        score = 0;
        scoreText.text = score.ToString();
    }

    public void Jump()
    {
        if (isPause) return; ;
        if (isDoubleJumping) return;
        else if (isJumping) isDoubleJumping = true;
        else isJumping = true;
        
        animator.SetInteger("motion", 1);

        if (afterJumpRoutine != null) StopCoroutine(afterJumpRoutine);

        movement.MoveStart(new Vector2(0, pet.localPosition.y + PetInfo.Instance.jumpHeight), PetInfo.Instance.jumpTime, () =>
        {
            afterJumpRoutine = StartCoroutine(AfterJump());
        });
    }

    IEnumerator AfterJump()
    {
        yield return new WaitForSeconds(PetInfo.Instance.keepTime);

        animator.SetInteger("motion", 2);
        movement.MoveStart(new Vector2(0, 0), PetInfo.Instance.fallTime, () =>
        {
            isJumping = false;
            isDoubleJumping = false;
            animator.SetInteger("motion", 0);
        }, isDoubleJumping);
    }

    public void Damage(float dam)
    {
        hp = hp - dam;
        if (hp < 0) hp = 0;
        if (hp > petInfo.hp[petLevel]) hp = petInfo.hp[petLevel];
        hpSlider.value = hp;

        if (hp <= 0) GameOver(false);
    }

    private void GetCoin(int score)
    {
        this.score += score;
        scoreText.text = this.score.ToString();
    }

    public void GameOver(bool isFalling)
    {
        StopAllCoroutines();

        if (!isFalling)
        {
            animator.SetInteger("motion", 3);
            movement.Stop();
            pet.localPosition = new Vector2(0, 0);
        }
        playMng.GameOver(score);
    }

    public void PowerMode(bool set)
    {
        if (set)
        {
            isPowerMode = set;
            animator.SetFloat("speed", 2);
        }
        else
        {
            StartCoroutine(WaitSeconds(2, () => isPowerMode = set));
            animator.SetFloat("speed", 1);
        }
    }

    public void Replay()
    {
        isJumping = false;
        isDoubleJumping = false;
        transform.position = defaultPos;
        pet.localPosition = new Vector2(0, 0);
        animator.SetInteger("motion", 0);
        animator.speed = 1;
        isPause = false;
        movement.Stop();

        InitValues();
    }

    public void IsPause(bool pause)
    {
        animator.speed = pause? 0 : 1;
        isPause = pause;
        movement.Pause(pause);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Fall")
        {
            if (isPowerMode) return;
            transform.position = new Vector2(col.gameObject.transform.position.x+col.gameObject.GetComponent<SpriteRenderer>().size.x/2, transform.position.y);
            animator.SetInteger("motion", 2);
            movement.MoveStart(new Vector2(0, -5), 3, null);
            GameOver(true);
            //Debug.Log("Die caused by falling");
        }
        else if (col.tag == "GMonster")
        {
            if (isPowerMode) return;
            if (isJumping)
            {
                return;
            }
            attackSound.Play();
            //Debug.Log("damaged by ground moster");
            Damage(monsterDamage);
        }
        else if (col.tag == "SMonster")
        {
            if (isPowerMode) return;
            attackSound.Play();
           // Debug.Log("damaged by sky moster");
            Damage(monsterDamage);
        }
        else if (col.tag == "Coin")
        {
            getSound.Play();
            col.GetComponent<Animator>().SetBool("isConsumed", true);
            GetCoin(Coin.Instance.score[Convert.ToInt32(col.name)]);
            playMng.CheckPowerMode("coin");
        }
        else if (col.tag == "Item")
        {
            getSound.Play();
            col.GetComponent<Animator>().SetBool("isConsumed", true);
            playMng.GetItem(col.name);
        }
        else if (col.tag == "Spell")
        {
            getSound.Play();
            col.GetComponent<Animator>().SetBool("isConsumed", true);
            playMng.GetSpellItem();
        }
    }

    IEnumerator WaitSeconds(float sec, Action action)
    {
        yield return new WaitForSeconds(sec);
        action();
    }
}
