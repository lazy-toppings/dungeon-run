﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapMng : MonoBehaviour
{
    public Text distanceText;

    public float speed;
    public bool isPlaying;

    private Transform bg;
    private Transform blocks;
    private Transform gMonsters;
    private List<Transform> sMonsters = new List<Transform>();
    private List<Transform> items = new List<Transform>();

    private readonly string bgPath = "Prefabs/Bg/";
    private readonly string blockPath = "Prefabs/Block/";
    private readonly string gMonsterPath = "Prefabs/GroundMonster/";
    private readonly string sMonsterPath = "Prefabs/SkyMonster/";
    private readonly string coinPath = "Prefabs/Coin/";
    private readonly string itemPath = "Prefabs/Item/";

    private int curMap;
    private int bgCnt;
    private int blockCnt;

    private bool isPowerMode;

    private int level;

    private float distance;

    private int petLevel;
    private Pet petInfo;

    private void Awake()
    {
        petLevel = User.Instance.havePet[User.Instance.cur.pet].level;
        petInfo = PetInfo.Instance.pets[User.Instance.cur.pet];

        level = 0;
        speed = Map.Instance.speed[level];
        isPlaying = true;
        distance = 0;
        bg = transform.Find("Bg");
        blocks = transform.Find("Blocks");
        gMonsters = transform.Find("GroundMonsters");
        sMonsters.Add(transform.Find("SkyMonster0"));
        sMonsters.Add(transform.Find("SkyMonster1"));
        items.Add(transform.Find("Item0"));
        items.Add(transform.Find("Item1"));
        items.Add(transform.Find("Item2"));

        curMap = User.Instance.cur.map;
        bgCnt = Map.Instance.bgCnt[curMap];
        blockCnt = Map.Instance.blockCnt[curMap];

        MakeBg(true);
    }

    void Update()
    {
        if (!isPlaying) return;
        transform.Translate(Vector2.left * speed * Time.deltaTime);

        distance += speed / 50;
        distanceText.text = $"{(int)distance}m";

        if (level < Map.Instance.maxLevel && distance >= Map.Instance.speedDistance[level])
        {
            level++;
            if (isPowerMode) speed = Map.Instance.speed[level] * 2;
            else speed = Map.Instance.speed[level];
        }

        if (bg.childCount == 0) return;

        Transform last = bg.GetChild(bg.childCount - 1);
        if (last.position.x + last.GetComponent<SpriteRenderer>().size.x < 25)
        {
            MakeBg();
        }
    }

    public void Replay()
    {
        isPlaying = false;
        level = 0;
        speed = Map.Instance.speed[level];
        distance = 0;
        transform.position = new Vector2(0, 0);
        ResetBg();

        MakeBg(true);
        isPlaying = true;
    }

    // returns new int[2] { exp, distance }
    public int[] GameOver(int attackCnt)
    {
        isPlaying = false;
        int exp = User.Instance.havePet[User.Instance.cur.pet].exp + (int)(attackCnt * petInfo.expControl[petLevel] * Distance.Instance.Bonus(distance));
        int max = petInfo.levelUpExp[petInfo.levelUpExp.Count - 1];
        if (exp > max) exp = max;
        int prevExp = User.Instance.havePet[User.Instance.cur.pet].exp;
        User.Instance.havePet[User.Instance.cur.pet].exp = exp;
        return new int[2] { exp - prevExp, (int)distance };
    }

    public void PowerModeStart()
    {
        isPowerMode = true;
        speed = Map.Instance.speed[level] * petInfo.powerModePower[petLevel];
    }

    public void PowerModeDone()
    {
        isPowerMode = false;
        speed = Map.Instance.speed[level];
    }

    private void MakeBg(bool isFirst = false)
    {
        float nextX = 0;
        if (bg.childCount != 0 && !isFirst)
        {
            Transform prev = bg.GetChild(bg.childCount - 1);
            nextX = prev.localPosition.x + prev.GetComponent<SpriteRenderer>().size.x;
        }

        GameObject b = Instantiate(Resources.Load($"{bgPath}{curMap}/{Random.Range(0, bgCnt)}")) as GameObject;
        b.transform.SetParent(bg);
        b.transform.localPosition = new Vector2(nextX, 0);

        MakeBlocks(b.transform, isFirst);
    }

    private int prevB = 0;
    private int prevMonster = -1;
    private void MakeBlocks(Transform nowBg, bool isFirst = false)
    {
        float nextX = 0;
        if (blocks.childCount != 0 && !isFirst)
        {
            Transform prev = blocks.GetChild(blocks.childCount - 1);
            nextX = prev.localPosition.x + prev.GetComponent<SpriteRenderer>().size.x;
        }

        while (nextX < nowBg.localPosition.x + nowBg.GetComponent<SpriteRenderer>().size.x / 2)
        {
            bool canMonster = prevB != -1 && !isFirst && prevMonster == -1;
            if (prevB == -1 || isFirst) prevB = Random.Range(0, blockCnt);
            else prevB = Random.Range(-1, blockCnt);
            prevMonster = -1;

            if (canMonster && prevB != -1)
            {
                prevMonster = MakeMonster(nextX);
            }
            if (!isFirst) MakeItem(nextX, prevMonster);

            GameObject b = Instantiate(Resources.Load($"{blockPath}{curMap}/{prevB}")) as GameObject;
            b.transform.SetParent(blocks);
            b.transform.localPosition = new Vector2(nextX, 0);
            nextX += b.GetComponent<SpriteRenderer>().size.x;
        }
    }

    private int MakeMonster(float xPos)
    {
        float make = Random.Range(0, 3);
        if (make > 1) return -1;

        int gors = Random.Range(0, 3);
        GameObject m;
        if (gors == 0)
        {
            m = Instantiate(Resources.Load($"{gMonsterPath}{User.Instance.cur.map}/0")) as GameObject;
            m.transform.SetParent(gMonsters);
        }
        else
        {
            m = Instantiate(Resources.Load($"{sMonsterPath}{User.Instance.cur.map}/0")) as GameObject;
            m.transform.SetParent(sMonsters[gors-1]);
        }
        m.transform.localPosition = new Vector2(xPos, 0);

        return gors;
    }

    private void MakeItem(float xPos, int impossiblePos)
    {
        List<int> possiblePos = new List<int> { 0, 1, 2 };
        possiblePos.Remove(impossiblePos);

        // make spell
        float makeSpell = Random.Range(0, 300);
        if (makeSpell == 0)
        {
            int sPos = possiblePos[Random.Range(0, possiblePos.Count)];

            GameObject spell = Instantiate(Resources.Load($"{itemPath}Spell")) as GameObject;
            spell.transform.SetParent(items[sPos]);
            spell.transform.localPosition = new Vector2(xPos, 0);

            possiblePos.Remove(sPos);
        }

        // make item
        int makeItem = Random.Range(0, petInfo.itemPr[petLevel]);
        if (makeItem == 0)
        {
            int iPos = possiblePos[Random.Range(0, possiblePos.Count)];
            string iKind = User.Instance.havePet[User.Instance.cur.pet].BulletItemWeigthed();

            GameObject item = Instantiate(Resources.Load($"{itemPath}{iKind}")) as GameObject;
            item.transform.SetParent(items[iPos]);
            item.transform.localPosition = new Vector2(xPos, 0);
            item.name = iKind;

            possiblePos.Remove(iPos);
        }

        // make coin
        int makeCoin = Random.Range(0, 2);
        if (makeCoin == 0)
        {
            int cPos = possiblePos[Random.Range(0, possiblePos.Count)];
            int cKind = Random.Range(0, 3);
            GameObject coin = Instantiate(Resources.Load($"{coinPath}{cKind}")) as GameObject;
            coin.name = cKind.ToString();
            coin.transform.SetParent(items[cPos]);

            coin.transform.localPosition = new Vector2(xPos, 0);
        }
    }

    private Transform GetGroundMonster(float pos)
    {
        foreach (Transform t in gMonsters)
        {
            if (t.position.x > pos) return t;
        }
        return null;
    }

    private Transform GetSky0Monster(float pos)
    {
        foreach (Transform t in sMonsters[0])
        {
            if (t.position.x > pos) return t;
        }
        return null;
    }

    public Transform GetFrontMonster(float xPos, float height)
    {
        switch (height)
        {
            case 0: return GetGroundMonster(xPos);
            case 1: return GetSky0Monster(xPos);
            case 2: return GetSky1Monster(xPos);
        }
        return null;
    }

    private Transform GetSky1Monster(float pos)
    {
        foreach (Transform t in sMonsters[1])
        {
            if (t.position.x > pos) return t;
        }
        return null;
    }

    private void ResetBg()
    {
        foreach (Transform t in bg) Destroy(t.gameObject);
        foreach (Transform t in blocks) Destroy(t.gameObject);
        foreach (Transform t in gMonsters) Destroy(t.gameObject);
        foreach (Transform p in sMonsters) foreach (Transform t in p) Destroy(t.gameObject);
        foreach (Transform p in items) foreach (Transform t in p) Destroy(t.gameObject);
    }
}
