﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundMng : MonoBehaviour
{
    public List<AudioSource> audios;

    void Start()
    {
        if (!User.Instance.setting.soundOn)
            foreach (AudioSource audio in audios) audio.volume = 0;
    }

}
