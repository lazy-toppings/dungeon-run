﻿using UnityEngine;
using System.Collections;

public class BulletCollider : MonoBehaviour
{
    public int idx;
    public BulletMng bulletMng;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Substring(1) == "Monster")
        {
            col.GetComponent<Animator>().SetBool("isDying", true);
            GetComponent<Movement>().Stop();
            gameObject.SetActive(false);
            bulletMng.available.Enqueue(idx);
            bulletMng.GetComponent<AudioSource>().Play();
            bulletMng.playMng.CheckPowerMode(col.tag.ToLower());
            bulletMng.playMng.AttackSuccess();
        }
    }
}
