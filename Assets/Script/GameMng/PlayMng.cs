﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;
using System.Linq;

public class PlayMng : MonoBehaviour
{
    public MapMng mapMng;
    public BulletMng bulletMng;
    public PetMng petMng;
    
    public SpriteAtlas itemAtlas;
    public SpriteAtlas bulletAtlas;
    public SpriteAtlas petAtlas;

    public Slider weaponSlider;
    public RectTransform weaponCapacity;
    public Image itemImg;
    public GameObject powerModeBtn;
    public GameObject gameOverPopup;
    public GameObject pausePopup;
    public Text attackText;
    public Text powerConditionText;

    public GameObject powerModeText;

    private Transform spellOpen;
    private RectTransform rank;

    private AudioSource gameOverSound;
    private AudioSource jumpSound;
    private AudioSource playSound;

    private readonly string capacityPath = "Prefabs/Capacity/";
    private readonly string rankPath = "Prefabs/UI/Rank";

    private Coroutine weaponResponed;
    private Coroutine hpDecrease;

    private bool isDying;
    private int bulletCnt;
    private bool isPause;
    private bool isPowerMode;

    private string item;

    private string powerModeType;
    private int powerModeCnt;

    private int myPowerModeCnt;
    private int attackCnt;

    private int petLevel;
    private Pet petInfo;

    private int bulletLevel;
    private Bullet bulletInfo;

    private int spellCnt;

    private void Start()
    {
        petLevel = User.Instance.havePet[User.Instance.cur.pet].level;
        petInfo = PetInfo.Instance.pets[User.Instance.cur.pet];
        bulletInfo = BulletInfo.Instance.bullets[$"{User.Instance.cur.pet}_{User.Instance.cur.bullet}"];
        bulletLevel = User.Instance.haveBullet[$"{User.Instance.cur.pet}_{User.Instance.cur.bullet}"].level;

        spellOpen = gameOverPopup.transform.Find("SpellOpen");
        rank = gameOverPopup.transform.Find("Rank").Find("View").Find("Viewport").Find("Content").GetComponent<RectTransform>();

        gameOverSound = transform.Find("GameOverSound").GetComponent<AudioSource>();
        jumpSound = transform.Find("JumpSound").GetComponent<AudioSource>();
        playSound = transform.Find("PlaySound").GetComponent<AudioSource>();

        InitValues();
    }

    public void Replay()
    {
        petMng.Replay();
        mapMng.Replay();
        gameOverPopup.SetActive(false);
        pausePopup.SetActive(false);
        playSound.Play();

        for (int i = 0; i < rank.childCount; ++i) Destroy(rank.GetChild(i).gameObject);

        if (weaponResponed != null) StopCoroutine(weaponResponed);
        if (hpDecrease != null) StopCoroutine(hpDecrease);
        InitValues();
    }

    public void ToHome()
    {
        SceneManager.LoadScene("HomeScene");
    }

    public void PauseTime()
    {
        if (mapMng.isPlaying)
        {
            petMng.IsPause(true);
            mapMng.isPlaying = false;
            isPause = true;
            pausePopup.SetActive(true);
        }
        else
        {
            petMng.IsPause(false);
            mapMng.isPlaying = true;
            isPause = false;
            pausePopup.SetActive(false);
        }
    }

    public void GameOver(int score)
    {
        if (weaponResponed != null) StopCoroutine(weaponResponed);
        if (hpDecrease != null) StopCoroutine(hpDecrease);
        int[] expNdistance = mapMng.GameOver(attackCnt);
        if (spellCnt > 0) ShowSpell();

        playSound.Stop();
        gameOverSound.Play();
        gameOverPopup.SetActive(true);
        Transform result = gameOverPopup.transform.Find("Result");
        result.Find("PetImg").GetComponent<Image>().sprite = petAtlas.GetSprite(User.Instance.cur.pet);
        result.Find("DistanceBonusText").GetComponent<Text>().text = $"x {Distance.Instance.Bonus(expNdistance[1])}";
        result.Find("PetBonusText").GetComponent<Text>().text = $"x {petInfo.coinControl[petLevel]}";
        result.Find("CoinText").GetComponent<Text>().text = $"{(int)(score * Distance.Instance.Bonus(expNdistance[1]) * petInfo.coinControl[petLevel])}";
        result.Find("ExpText").GetComponent<Text>().text = expNdistance[0].ToString();

        foreach (Transform t in bulletMng.transform) t.gameObject.SetActive(false);

        User.Instance.money += (int)(score * Distance.Instance.Bonus(expNdistance[1]) * petInfo.coinControl[petLevel]);
        User.Instance.AddRank(expNdistance[1]);
        float nextY = 0; int cnt = 0;
        rank.localPosition = new Vector2(0, 0);
        foreach (UserRank r in User.Instance.rank)
        {
            cnt++;
            RectTransform t = (Instantiate(Resources.Load(rankPath)) as GameObject).GetComponent<RectTransform>();
            t.SetParent(rank);
            t.localPosition = new Vector2(0, nextY);
            nextY -= t.sizeDelta.y;
            t.Find("RankNumText").GetComponent<Text>().text = $"{cnt}.";
            t.Find("PetImg").GetComponent<Image>().sprite = petAtlas.GetSprite(r.pet);
            t.Find("MeterText").GetComponent<Text>().text = $"{r.meter}m";
            if (r.pet == User.Instance.cur.pet && r.meter == expNdistance[1])
            {
                t.Find("Inactive").gameObject.SetActive(false);
                rank.localPosition = new Vector2(0, -nextY - 3 * t.sizeDelta.y);
            }
            if (rank.sizeDelta.y < -nextY) rank.sizeDelta = new Vector2(rank.sizeDelta.x, -nextY);
        }

        isDying = true;
    }

    IEnumerator BulletResponded()
    {
        while (true)
        {
            float time = 0;
            weaponSlider.value = time;
            while (time < 1)
            {
                yield return new WaitForSeconds(Time.deltaTime);
                if (isPause) continue;
                time += Time.deltaTime / bulletInfo.regenTime[bulletLevel];
                weaponSlider.value = time;
            }
            bulletCnt++;
            if (bulletCnt > bulletInfo.maxCapacity[bulletLevel]) bulletCnt = bulletInfo.maxCapacity[bulletLevel];
            SetWeaponCapacity();
        }
    }

    IEnumerator HpDecrease()
    {
        float interval = petInfo.hpInterval[petLevel];
        float decrease = petInfo.hpDecrease[petLevel];

        while (true)
        {
            yield return new WaitForSeconds(interval);
            if (isPause || isPowerMode) continue;
            petMng.Damage(decrease);
        }
    }

    public void GetItem(string id)
    {
        item = id;
        itemImg.sprite = itemAtlas.GetSprite(item);
    }

    public void UseItem()
    {
        if (item == "-1") return;
        else if (item[0] == 'B') // bulletItem
        {
            int tmp = bulletCnt + BulletItem.Instance.giveCnt[item[1] - '0'];
            if (tmp > bulletInfo.maxCapacity[bulletLevel]) tmp = bulletInfo.maxCapacity[bulletLevel];
            bulletCnt = tmp;
            SetWeaponCapacity();
        }
        else if (item[0] == 'H') // hpItem
        {
            petMng.Damage(-HpItem.Instance.giveHp[item[1] - '0']);
        }

        item = "-1";
        itemImg.sprite = itemAtlas.GetSprite(item.ToString());
    }

    public void GetSpellItem()
    {
        spellCnt++;
    }

    private void InitValues()
    {
        powerModeBtn.SetActive(false);

        myPowerModeCnt = 0;
        isPowerMode = false;
        SetPowerMode();

        attackCnt = 0;
        attackText.text = attackCnt.ToString();

        item = "-1";
        itemImg.sprite = itemAtlas.GetSprite(item.ToString());

        isDying = false;
        isPause = false;
        bulletCnt = bulletInfo.maxCapacity[bulletLevel];
        InitWeaponCapacity();

        spellCnt = 0;

        weaponResponed = StartCoroutine(BulletResponded());
        hpDecrease = StartCoroutine(HpDecrease());
    }

    public void CheckPowerMode(string str)
    {
        if (powerModeText.activeSelf) return;
        if ((str == powerModeType || str.Substring(1) == powerModeType) && !powerModeBtn.activeSelf && !isPowerMode)
        {
            myPowerModeCnt++;
            SetPowerConditionText();
            if (myPowerModeCnt == powerModeCnt)
            {
                powerModeBtn.SetActive(true);
                myPowerModeCnt = 0;
                powerConditionText.color = new Color(0.972549f, 0.7647059f, 0.07843138f);
            }
        }
    }

    public void Jump()
    {
        if (isDying) return;
        jumpSound.Play();
        petMng.Jump();
    }

    public void Attack()
    {
        if (isPause) return;
        if (bulletCnt == 0) return;
        if (isDying) return;
        bulletMng.Shoot(petMng.pet.GetComponent<SpriteRenderer>().bounds.center.y);

        bulletCnt--;
        SetWeaponCapacity();
    }

    public void AttackSuccess()
    {
        attackCnt += Monster.Instance.exp;
        attackText.text = attackCnt.ToString();
    }

    private void ShowSpell()
    {
        spellCnt--;
        spellOpen.gameObject.SetActive(true);
        spellOpen.GetChild(0).gameObject.SetActive(true);
    }

    // at the game over screen
    public void GetSpell()
    {
        spellOpen.GetChild(0).gameObject.SetActive(false);
        spellOpen.GetChild(1).gameObject.SetActive(true);

        int idx = Random.Range(0, User.Instance.havePet.Count);
        string randomPet = User.Instance.havePet.Keys.ToList()[idx];
        int randomBullet = Random.Range(0, BulletInfo.Instance.petBullet[randomPet].Count);

        string getSpell = $"{randomPet}_{randomBullet}";
        spellOpen.GetChild(1).Find("SpellImg").GetComponent<Image>().sprite = bulletAtlas.GetSprite(getSpell);
        if (User.Instance.haveBullet.ContainsKey(getSpell)) User.Instance.haveBullet[getSpell].cnt++;
        else User.Instance.haveBullet.Add(getSpell, new UserBullet(getSpell));
    }

    public void GetSpellGone()
    {
        spellOpen.GetChild(1).gameObject.SetActive(false);
        spellOpen.gameObject.SetActive(false);
        if (spellCnt > 0) ShowSpell();
    }

    private void InitWeaponCapacity()
    {
        float nextX = 0; float nextY = 0;
        for (int i = 0; i < bulletCnt; ++i)
        {
            GameObject g = Instantiate(Resources.Load($"{capacityPath}{User.Instance.cur.pet}_{User.Instance.cur.bullet}")) as GameObject;
            g.transform.SetParent(weaponCapacity);
            g.transform.localScale = new Vector2(1, 1);
            g.transform.localPosition = new Vector2(nextX, nextY);
            nextX += g.GetComponent<RectTransform>().sizeDelta.x + 10;
            if (nextX > weaponCapacity.sizeDelta.x)
            {
                nextX = 0; nextY = -g.GetComponent<RectTransform>().sizeDelta.y-2;
            }
        }
    }

    private void SetWeaponCapacity()
    {
        for (int i = 0; i < weaponCapacity.childCount; ++i)
        {
            weaponCapacity.GetChild(i).gameObject.SetActive(i < bulletCnt);
        }
    }

    public void StartPowerMode()
    {
        isPowerMode = true;
        powerModeBtn.SetActive(false);
        petMng.PowerMode(true);
        powerModeText.SetActive(true);
        mapMng.PowerModeStart();
        StartCoroutine(PowerModeRoutine());
    }

    private void SetPowerMode()
    {
        powerModeType = PowerMode.Instance.type[Random.Range(0, PowerMode.Instance.type.Count)];
        powerModeCnt = PowerMode.Instance.cnt[Random.Range(0, PowerMode.Instance.cnt.Count)];

        SetPowerConditionText();
        powerConditionText.color = new Color(239, 239, 239);
    }

    private void SetPowerConditionText()
    {
        if (powerModeType == "monster")
            powerConditionText.text = $"Kill {powerModeCnt} monsters ({myPowerModeCnt}/{powerModeCnt})";
        else if (powerModeType == "coin")
            powerConditionText.text = $"Get {powerModeCnt} coins ({myPowerModeCnt}/{powerModeCnt})";
    }

    IEnumerator PowerModeRoutine()
    {
        float interval = petInfo.powerModeShootInterval[petLevel];
        float time = interval;
        while (time < petInfo.powerModeTime[petLevel])
        {
            bulletMng.Shoot(petMng.pet.GetComponent<SpriteRenderer>().bounds.center.y, 1.3f);
            yield return new WaitForSeconds(interval);
            time += interval;
        }
        isPowerMode = false;
        powerModeText.SetActive(false);
        petMng.PowerMode(false);
        mapMng.PowerModeDone();
        SetPowerMode();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Jump();
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            Attack();
        }
    }

    private void OnApplicationQuit()
    {
        User.Instance.Save();
    }
}
