﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Text;
using Newtonsoft.Json;

public class SettingMng : MonoBehaviour
{
    private static string baseUrl = $"{ServerUtil.SERVER_URL}/v1/dungeonrun/data";

    public StripeTokenMng stripeTokenMng;

    public GameObject loading;

    public AudioSource sound;
    public Toggle onToggle;
    public Toggle offToggle;

    private void Awake()
    {
        if (User.Instance.cur.pet == "-1") User.Instance.Set(SaveLoadMng.LoadData());
        if (User.Instance.setting.soundOn)
        {
            onToggle.isOn = true;
            offToggle.isOn = false;
            sound.Play();
        }
        else
        {
            onToggle.isOn = false;
            offToggle.isOn = true;
            sound.Stop();
        }
    }

    public void SoundON(bool isOn)
    {
        if (isOn)
        {
            if (!User.Instance.setting.soundOn) sound.Play();
            User.Instance.setting.soundOn = true;
        }
    }

    public void SoundOff(bool isOn)
    {
        if (isOn)
        {
            User.Instance.setting.soundOn = false;
            sound.Stop();
        }
    }

    public void LoadData()
    {
        if (User.Instance.id == null) stripeTokenMng.Open("signup");
        else
        {
            loading.SetActive(true);
            StartCoroutine(DataGetRoutine());
        }
    }

    public void SaveData()
    {
        if (User.Instance.id == null) stripeTokenMng.Open("signup");
        else
        {
            loading.SetActive(true);
            StartCoroutine(DataPostRoutine());
        }
    }

    IEnumerator DataPostRoutine()
    {
        string url = baseUrl;
        Debug.Log(url);

        var jsonData = User.Instance.ToJson();
        Debug.Log(jsonData);

        UnityWebRequest www = UnityWebRequest.Post(url, jsonData);
        www.SetRequestHeader("Accept", "application/json");
        www.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonData));
        www.uploadHandler.contentType = "application/json";

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            // TODO post fail popup
            loading.SetActive(false);
            Debug.Log(www.error);
        }
        else
        {
            loading.SetActive(false);
            Debug.Log("Form upload complete!");
        }
    }

    IEnumerator DataGetRoutine()
    {
        string url = $"{baseUrl}/{User.Instance.id}";
        UnityWebRequest www = UnityWebRequest.Get(url);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            loading.SetActive(false);
        }
        else
        {
            loading.SetActive(false);
            string data = www.downloadHandler.text;
            Debug.Log(data);
            User.Instance.Set(JsonConvert.DeserializeObject<User>(data));
            User.Instance.Save();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
