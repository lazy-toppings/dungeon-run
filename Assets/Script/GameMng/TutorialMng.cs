﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialMng : MonoBehaviour
{
    public CurInfoMng curInfoMng;

    public Transform checkmark;
    public Transform toggles;

    public int select = 0;

    private void Awake()
    {
        if (User.Instance.cur.pet == "-1") User.Instance.Set(SaveLoadMng.LoadData());
        if (User.Instance.setting.tutoOn) transform.GetChild(0).gameObject.SetActive(true);
    }

    private void Start()
    {
        for (int i = 0; i < 3; ++i)
        {
            int idx = i;
            toggles.GetChild(i).GetComponent<Toggle>().onValueChanged.AddListener((isOn) => ToggleChanged(isOn, idx));
        }
    }

    public void ToggleChanged(bool isOn, int idx)
    {
        if (isOn) checkmark.GetChild(idx).gameObject.SetActive(true);
        else checkmark.GetChild(idx).gameObject.SetActive(false);
        select = idx;
    }

    public void Play()
    {
        switch (select)
        {
            case 0: select = 2; break;
            case 1: select = 5; break;
            case 2: select = 13; break;
        }
        string pet = select.ToString();
        User.Instance.havePet.Add(pet, new UserPet(pet));
        User.Instance.haveBullet.Add($"{pet}_0", new UserBullet($"{pet}_0"));
        User.Instance.haveMap.Add("0");
        User.Instance.cur.pet = pet;
        transform.GetChild(0).gameObject.SetActive(false);

        User.Instance.setting.tutoOn = false;
        curInfoMng.SetImgs();
    }
}
