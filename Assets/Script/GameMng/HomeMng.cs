﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class HomeMng : MonoBehaviour
{
    public StripeTokenMng stripeTokenMng;
    public RegisterChainMng registerChainMng;

    public SpriteAtlas standPetAtlas;
    public SpriteAtlas bulletAtlas;
    public SpriteAtlas mapAtlas;
    public SpriteAtlas featureAtlas;

    public CurInfoMng curInfoMng;
    public Transform petSelect;
    public Transform petInfo;
    public Transform bulletInfo;
    public Transform mapBuy;
    public Image mapImg;
    public Text moneyText;
    public SpriteRenderer bg;
    public Transform alarm;
    public GameObject loading;
    public GameObject dataLoadAlarm;

    private readonly string petSelectPath = "Prefabs/UI/PetSelect";
    private readonly string bulletSelectPath = "Prefabs/UI/BulletSelect";
    private readonly string mapSelectPath = "Prefabs/UI/MapSelect";

    private RectTransform petList;
    private RectTransform bulletList;
    private RectTransform mapList;

    private Image petImg;
    private Text pLvText;
    private Slider pLvSlider;
    private Text pInfoText;
    private Button pLevelUpBtn;
    private Text pLevelUpText;
    private Text pExpText;
    private Image pFeatureImg;

    private Image bulletImg;
    private Text bLvText;
    private Slider bLvSlider;
    private Text bCntText;
    private Text bInfoText;
    private Button bLevelUpBtn;
    private Text bLevelUpText;

    private GameObject sendText;
    private GameObject waitText;
    private GameObject get;

    private Coroutine waitRoutine;
    
    private void Start()
    {        
        Debug.Log(User.Instance.id);
        Debug.Log(SystemInfo.deviceUniqueIdentifier);
        Application.RequestAdvertisingIdentifierAsync(
            (string advertisingId, bool trackingEnabled, string error) =>
            { Debug.Log("advertisingId " + advertisingId + " " + trackingEnabled + " " + error); }
        );

        petList = petSelect.Find("PetList").Find("Viewport").Find("Content").GetComponent<RectTransform>();
        bulletList = petSelect.Find("BulletList").Find("Viewport").Find("Content").GetComponent<RectTransform>();
        mapList = mapBuy.Find("MapList").Find("Viewport").Find("Content").GetComponent<RectTransform>();

        Transform pBbg = petInfo.Find("Bg");
        petImg = pBbg.Find("PetImg").GetComponent<Image>();
        pLvText = pBbg.Find("LvText").GetComponent<Text>();
        pLvSlider = pBbg.Find("LvSlider").GetComponent<Slider>();
        pInfoText = pBbg.Find("InfoText").GetComponent<Text>();
        pLevelUpBtn = pBbg.Find("LevelUpBtn").GetComponent<Button>();
        pLevelUpText = pLevelUpBtn.GetComponentInChildren<Text>();
        pExpText = pBbg.Find("ExpText").GetComponent<Text>();
        pFeatureImg = pBbg.Find("FeatureImg").GetComponent<Image>();

        Transform bBbg = bulletInfo.Find("Bg");
        bulletImg = bBbg.Find("BulletImg").GetComponent<Image>();
        bLvText = bBbg.Find("LvText").GetComponent<Text>();
        bLvSlider = bBbg.Find("LvSlider").GetComponent<Slider>();
        bInfoText = bBbg.Find("InfoText").GetComponent<Text>();
        bLevelUpBtn = bBbg.Find("LevelUpBtn").GetComponent<Button>();
        bLevelUpText = bLevelUpBtn.GetComponentInChildren<Text>();
        bCntText = bBbg.Find("CntText").GetComponent<Text>();

        Transform sendBtn = petInfo.Find("SendBtn");
        sendText = sendBtn.Find("SendText").gameObject;
        waitText = sendBtn.Find("WaitText").gameObject;
        get = sendBtn.Find("Get").gameObject;

        SetMap();
        SetMoney();

        LoadChain();
    }

    public void Play()
    {
        if (User.Instance.cur.bullet == "-1")
        {
            alarm.gameObject.SetActive(true);
            alarm.GetComponentInChildren<Text>().text = "You can not run without any spell. Run with other pet, and get spell first.";
            StartCoroutine(WaitSeconds(3, () => alarm.gameObject.SetActive(false)));
        }
        else SceneManager.LoadScene("PlayScene");
    }

    public void Select()
    {
        petSelect.gameObject.SetActive(true);

        LoadPetList();
        LoadBulletList(User.Instance.cur.pet);
    }

    public void MapLoad()
    {
        mapBuy.gameObject.SetActive(true);

        LoadMapList();
    }

    private void LoadPetList()
    {
        if (petList.childCount != 0) return;

        int i = 0;
        int nextY = 0; int nextX = 0;
        ToggleGroup tg = petList.GetComponent<ToggleGroup>();
        foreach (string pet in PetInfo.Instance.possible)
        {
            int xDelta = 0;
            int yDelta = 0;

            Transform t = Instantiate(Resources.Load<Transform>(petSelectPath));
            t.SetParent(petList);
            t.Find("PetImg").GetComponent<Image>().sprite = standPetAtlas.GetSprite(pet);

            Transform inactive = t.Find("Inactive");
            if (!User.Instance.havePet.ContainsKey(pet) || !User.Instance.havePet[pet].Active())
            {
                int cost = PetInfo.Instance.pets[pet].levelUpCost[0];
                if (cost == 0)
                {
                    inactive.Find("InfoText").gameObject.SetActive(true);
                    inactive.Find("InfoText").GetComponent<Text>().text = $"You can buy a pet in {PetInfo.Instance.pets[pet].origin}";
                }
                else
                {
                    Transform buyBtn = inactive.Find("BuyBtn");
                    buyBtn.gameObject.SetActive(true);
                    buyBtn.GetComponent<Button>().onClick.AddListener(() => BuyPet(pet, cost, inactive.gameObject));
                    buyBtn.Find("CostText").GetComponent<Text>().text = cost.ToString();
                }
            }
            else
            {
                inactive.gameObject.SetActive(false);
            }
            Button btn = t.Find("InfoBtn").GetComponent<Button>();
            btn.onClick.AddListener(() => LoadPetInfo(pet));

            Toggle toggle = t.GetComponentInChildren<Toggle>();
            toggle.group = tg;
            toggle.onValueChanged.AddListener((isSelected) => ChangePet(isSelected, pet, btn.gameObject));
            if (pet == User.Instance.cur.pet)
            {
                toggle.isOn = true;
                btn.gameObject.SetActive(true);
            }
            else
            {
                btn.gameObject.SetActive(false);
            }
            if (i % 3 == 0 && -nextY + t.GetComponent<RectTransform>().sizeDelta.y > petList.sizeDelta.y) petList.sizeDelta = new Vector2(petList.sizeDelta.x, -nextY + t.GetComponent<RectTransform>().sizeDelta.y);

            if (i % 3 == 2)
            {
                xDelta = -2 * (int)t.GetComponent<RectTransform>().sizeDelta.x;
                yDelta = -(int)t.GetComponent<RectTransform>().sizeDelta.y;
            }
            else
            {
                xDelta = (int)t.GetComponent<RectTransform>().sizeDelta.x;
            }
            
            i++;
            t.localScale = new Vector2(1, 1);
            t.localPosition = new Vector2(nextX, nextY);
            nextX += xDelta;
            nextY += yDelta;
        }
    }

    private void LoadBulletList(string pet, bool reload = false)
    {
        if (!reload && bulletList.childCount != 0) return;
        else if (reload) foreach (Transform t in bulletList) Destroy(t.gameObject);

        int i = 0;
        int nextY = 0;
        ToggleGroup tg = bulletList.GetComponent<ToggleGroup>();
        foreach (string b in BulletInfo.Instance.petBullet[pet])
        {
            int xDelta = 0;
            int yDelta = 0;

            Transform t = Instantiate(Resources.Load<Transform>(bulletSelectPath));
            t.SetParent(bulletList);
            t.Find("BulletImg").GetComponent<Image>().sprite = bulletAtlas.GetSprite($"{pet}_{b}");

            if (User.Instance.haveBullet.ContainsKey($"{pet}_{b}")) t.Find("Inactive").gameObject.SetActive(false);

            Button btn = t.Find("InfoBtn").GetComponent<Button>();
            btn.onClick.AddListener(() => LoadBulletInfo($"{pet}_{b}"));

            Toggle toggle = t.GetComponentInChildren<Toggle>();
            toggle.group = tg;
            toggle.onValueChanged.AddListener((isSelected) => ChangeBullet(isSelected, b, btn.gameObject));
            if (b == User.Instance.cur.bullet)
            {
                btn.gameObject.SetActive(true);
                toggle.isOn = true;
            }
            else
            {
                btn.gameObject.SetActive(false);
            }
            if (i % 2 == 1)
            {
                yDelta = -(int)t.GetComponent<RectTransform>().sizeDelta.y;
                xDelta = (int)t.GetComponent<RectTransform>().sizeDelta.x;
            }
            t.localPosition = new Vector2(xDelta, nextY);

            i++;
            t.localScale = new Vector2(1, 1);
            nextY += yDelta;
            if (-nextY > bulletList.sizeDelta.y) bulletList.sizeDelta = new Vector2(bulletList.sizeDelta.x, -nextY);
        }
    }

    private void LoadMapList()
    {
        if (mapList.childCount != 0) return;

        int nextX = 0;
        foreach (string map in Map.Instance.possible)
        {
            Transform t = Instantiate(Resources.Load<Transform>(mapSelectPath));
            t.SetParent(mapList);
            t.Find("MapImg").GetComponent<Image>().sprite = mapAtlas.GetSprite(map.ToString());

            Transform inactive = t.Find("Inactive");
            if (User.Instance.haveMap.Contains(map)) inactive.gameObject.SetActive(false);
            else
            {
                Button b = inactive.Find("BuyBtn").GetComponent<Button>();
                b.onClick.AddListener(() => BuyMap(map, inactive.gameObject));
                b.transform.Find("CostText").GetComponent<Text>().text = Map.Instance.mapCost[map].ToString();

                if (User.Instance.money < Map.Instance.mapCost[map]) b.interactable = false;
            }

            t.localScale = new Vector2(1, 1);
            t.localPosition = new Vector2(nextX, 0);
            nextX += (int)t.GetComponent<RectTransform>().sizeDelta.x;

            if (nextX > mapList.sizeDelta.x) mapList.sizeDelta = new Vector2(nextX, mapList.sizeDelta.y);
        }
    }

    private void BuyPet(string pet, int cost, GameObject inactive)
    {
        if (User.Instance.money < cost)
        {
            if (User.Instance.id == null) stripeTokenMng.Open("signup");
            else stripeTokenMng.Open(pet, () => { inactive.SetActive(false); });
            
            return;
        }
        User.Instance.money -= cost;
        SetMoney();

        User.Instance.havePet.Add(pet, new UserPet(pet));
        inactive.SetActive(false);
    }

    private void BuyMap(string map, GameObject inactive)
    {
        User.Instance.money -= Map.Instance.mapCost[map];
        User.Instance.haveMap.Add(map);

        inactive.SetActive(false);
        SetMoney();
    }

    public void NextMap()
    {
        int map = User.Instance.cur.map + 1;
        if (map == User.Instance.haveMap.Count) map = 0;
        
        User.Instance.cur.map = map;
        SetMap();
    }

    private void SetMap()
    {
        mapImg.sprite = mapAtlas.GetSprite(User.Instance.cur.map.ToString());
        bg.sprite = mapAtlas.GetSprite(User.Instance.cur.map.ToString());
    }

    private void SetMoney()
    {
        moneyText.text = User.Instance.money.ToString();
    }

    private void ChangePet(bool isSelected, string pet, GameObject btn)
    {
        if (!isSelected)
        {
            btn.SetActive(false);
        }
        else
        {
            if (User.Instance.cur.pet != pet)
            {
                bool find = false;
                User.Instance.cur.pet = pet;
                foreach (string b in BulletInfo.Instance.petBullet[pet])
                    if (User.Instance.haveBullet.ContainsKey($"{pet}_{b}"))
                    {
                        User.Instance.cur.bullet = b;
                        find = true;
                        break;
                    }
                if (!find)
                {
                    User.Instance.cur.bullet = "-1";
                }
            }
            btn.SetActive(true);
            LoadBulletList(pet, true);
        }
    }

    private void LoadPetInfo(string p)
    {
        petInfo.gameObject.SetActive(true);
        petImg.sprite = standPetAtlas.GetSprite(p);
        UserPet info = User.Instance.havePet[p];
        Pet metaPetInfo = PetInfo.Instance.pets[p];
        pLvText.text = $"Lv.{info.level+1}";
        
        pLvSlider.maxValue = metaPetInfo.levelUpExp[info.level + 1];
        pExpText.text = $"{info.exp} / {pLvSlider.maxValue}";
        pLvSlider.value = info.exp;
        pInfoText.text = $"{metaPetInfo.hp[info.level]}\n{1 / metaPetInfo.itemPr[info.level] * 100}%\nx {metaPetInfo.expControl[info.level]}\n{(int)((metaPetInfo.damageControl[info.level]-0.8f)*100)}%";
        pFeatureImg.sprite = featureAtlas.GetSprite(metaPetInfo.feature);

        pLevelUpBtn.onClick.RemoveAllListeners();
        pLevelUpBtn.onClick.AddListener(() => PetLevelUp(p, info.level));
        if (info.level < metaPetInfo.maxLevel && info.exp >= pLvSlider.maxValue && User.Instance.money >= metaPetInfo.levelUpCost[info.level+1]) pLevelUpBtn.interactable = true;
        else pLevelUpBtn.interactable = false;

        if (info.level == metaPetInfo.maxLevel) pLevelUpText.text = "max level";
        else pLevelUpText.text = $"Level Up {metaPetInfo.levelUpCost[info.level + 1]}";

        //TODO: delete test code
        petInfo.Find("TestLevelUp").GetComponent<Button>().onClick.RemoveAllListeners();
        petInfo.Find("TestLevelUp").GetComponent<Button>().onClick.AddListener(() =>
        {
            if (User.Instance.havePet[p].level == metaPetInfo.maxLevel) return;
            User.Instance.havePet[p].level++;
            User.Instance.havePet[p].CalTotalWeight(true);
            LoadPetInfo(p);
        });
        petInfo.Find("TestLevelDown").GetComponent<Button>().onClick.RemoveAllListeners();
        petInfo.Find("TestLevelDown").GetComponent<Button>().onClick.AddListener(() =>
        {
            if (User.Instance.havePet[p].level == 0) return;
            User.Instance.havePet[p].level--;
            User.Instance.havePet[p].CalTotalWeight(true);
            LoadPetInfo(p);
        });

        SetSend(p);
    }

    private void LoadBulletInfo(string b)
    {
        bulletInfo.gameObject.SetActive(true);
        bulletImg.sprite = bulletAtlas.GetSprite(b);
        UserBullet info = User.Instance.haveBullet[b];
        Bullet metaBulletInfo = BulletInfo.Instance.bullets[b];

        bLvText.text = $"Lv.{info.level + 1}";
        bInfoText.text = $"{metaBulletInfo.attackDistance[info.level]}m\n{metaBulletInfo.maxCapacity[info.level]}\n{metaBulletInfo.regenTime[info.level]}s";

        bLvSlider.maxValue = metaBulletInfo.levelUpCnt[info.level + 1];
        bCntText.text = $"{info.cnt} / {bLvSlider.maxValue}";
        bLvSlider.value = info.cnt;

        bLevelUpBtn.onClick.AddListener(() => BulletLevelUp(b, info.level));
        if (info.level < metaBulletInfo.maxLevel && info.cnt >= bLvSlider.maxValue && User.Instance.money >= metaBulletInfo.levelUpCost[info.level + 1]) bLevelUpBtn.interactable = true;
        else bLevelUpBtn.interactable = false;

        if (info.level == metaBulletInfo.maxLevel) bLevelUpText.text = "max level";
        else bLevelUpText.text = $"Level Up {metaBulletInfo.levelUpCost[info.level + 1]}";

        //TODO: Delete test code
        bulletInfo.Find("TestLevelUp").GetComponent<Button>().onClick.RemoveAllListeners();
        bulletInfo.Find("TestLevelUp").GetComponent<Button>().onClick.AddListener(() =>
        {
            if (User.Instance.haveBullet[b].level == metaBulletInfo.maxLevel) return;
            User.Instance.haveBullet[b].level++;
            LoadBulletInfo(b);
        });
        bulletInfo.Find("TestLevelDown").GetComponent<Button>().onClick.RemoveAllListeners();
        bulletInfo.Find("TestLevelDown").GetComponent<Button>().onClick.AddListener(() =>
        {
            if (User.Instance.haveBullet[b].level == 0) return;
            User.Instance.haveBullet[b].level++;
            LoadBulletInfo(b);
        });
    }

    public void SetSend(string p)
    {
        Button sendBtn = petInfo.Find("SendBtn").GetComponent<Button>();
        sendBtn.onClick.RemoveAllListeners();

        if (User.Instance.havePet[p].work.isWork)
        {
            TimeSpan remain = DateTime.Now.Subtract(User.Instance.havePet[p].work.workTime);
            if (remain > Work.Instance.workDistance)
            {
                User.Instance.havePet[p].work.isReward = true;
                User.Instance.havePet[p].work.isWork = false;
            }
        }
        
        if (User.Instance.id == null)
        {
            waitText.SetActive(false);
            get.SetActive(false);

            sendText.SetActive(true);
            sendBtn.interactable = true;
            sendBtn.onClick.AddListener(() =>
            {
                stripeTokenMng.Open("signup", () => SetSend(p));
            });
        }
        else if (!(User.Instance.havePet[p].onChain || User.Instance.havePet[p].pass))
        {
            waitText.SetActive(false);
            get.SetActive(false);

            sendText.SetActive(true);
            sendBtn.interactable = true;
            sendBtn.onClick.AddListener(() =>
            {
                stripeTokenMng.Open(p, () =>
                {
                    SetSend(p);
                });
            });
        }
        else if (User.Instance.havePet[p].work.isReward)
        {
            sendText.SetActive(false);
            waitText.SetActive(false);

            get.SetActive(true);
            sendBtn.interactable = true;
            get.GetComponentInChildren<Text>().text = Work.Instance.getExp.ToString();
            sendBtn.onClick.AddListener(() =>
            {
                User.Instance.havePet[p].exp += Work.Instance.getExp;
                User.Instance.havePet[p].work.isReward = false;
                LoadPetInfo(p);
            });
        }
        else if (User.Instance.havePet[p].work.isWork)
        {
            sendText.SetActive(false);
            get.SetActive(false);

            waitText.SetActive(true);
            sendBtn.interactable = false;
            waitRoutine = StartCoroutine(WorkWaitTimeShow(p));
        }
        else
        {
            waitText.SetActive(false);
            get.SetActive(false);

            sendText.SetActive(true);
            sendBtn.interactable = true;
            sendBtn.onClick.AddListener(() =>
            {
                sendBtn.interactable = false;
                get.SetActive(false);

                User.Instance.havePet[p].work.isWork = true;
                User.Instance.havePet[p].work.workTime = DateTime.Now;
                waitRoutine = StartCoroutine(WorkWaitTimeShow(p));
                waitText.SetActive(true);
                sendText.SetActive(false);
            });
        }
    }

    IEnumerator WorkWaitTimeShow(string p)
    {
        TimeSpan remain = Work.Instance.workDistance.Subtract(DateTime.Now.Subtract(User.Instance.havePet[p].work.workTime));
        Text timeText = waitText.GetComponent<Text>();

        while (remain.TotalSeconds > 0)
        {
            timeText.text = remain.ToString(@"hh\:mm\:ss");
            yield return new WaitForSeconds(1);
            remain = remain.Subtract(new TimeSpan(0, 0, 1));
        }
        SetSend(p);
    }

    public void ClosePetInfo()
    {
        petInfo.gameObject.SetActive(false);
        if (waitRoutine != null) StopCoroutine(waitRoutine);
    }

    public void CloseBulletInfo()
    {
        bulletInfo.gameObject.SetActive(false);
    }

    private void ChangeBullet(bool isSelected, string b, GameObject btn)
    {
        if (!isSelected)
        {
            btn.SetActive(false);
        }
        else
        {
            btn.SetActive(true);
            User.Instance.cur.bullet = b;
        }
    }

    private void PetLevelUp(string p, int level)
    {
        User.Instance.money -= PetInfo.Instance.pets[p].levelUpCost[level + 1];
        User.Instance.havePet[p].level++;
        User.Instance.havePet[p].CalTotalWeight(true);
        LoadPetInfo(p);
        SetMoney();
    }

    private void BulletLevelUp(string b, int level)
    {
        User.Instance.money -= BulletInfo.Instance.bullets[b].levelUpCost[level + 1];
        User.Instance.haveBullet[b].level++;
        LoadBulletInfo(b);
        SetMoney();
    }
    
    public void CloseSelect()
    {
        petSelect.gameObject.SetActive(false);
        curInfoMng.SetImgs();
    }

    public void CloseMapBuy()
    {
        mapBuy.gameObject.SetActive(false);
    }

    private void OnApplicationQuit()
    {
        User.Instance.Save();
    }

    public void TestResetData()
    {
        User.Instance.Set(null);
        User.Instance.Save();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void TestOpenAll()
    {
        User.Instance.money += 10000000;
        foreach (KeyValuePair<string, List<string>> pb in BulletInfo.Instance.petBullet)
            foreach (string bulls in pb.Value)
            {
                string b = $"{pb.Key}_{bulls}";
                if (!User.Instance.haveBullet.ContainsKey(b)) User.Instance.haveBullet.Add(b, new UserBullet(b));
            }
        SetMoney();
    }

    IEnumerator WaitSeconds(float sec, Action action)
    {
        yield return new WaitForSeconds(sec);
        action();
    }

    private void LoadChain()
    {
        if (User.Instance.notFirst) return;

        User.Instance.notFirst = true;
        if (User.Instance.id != null)
        {
            AuthTokenMng authTokenMng = FindObjectOfType<AuthTokenMng>();
            AssetListMng assetListMng = FindObjectOfType<AssetListMng>();
            dataLoadAlarm.SetActive(true);
            authTokenMng.GetAuthToken(authTokenResponse =>
            {
                assetListMng.GetAssetList(authTokenResponse.Token, User.Instance.id, response =>
                {
                    dataLoadAlarm.SetActive(false);
                    SyncChainPet(response);
                    Debug.Log(response); 
                }, error =>
                {
                    dataLoadAlarm.SetActive(false);
                    Debug.Log(error);
                });
            }, error =>
            {
                dataLoadAlarm.SetActive(false);
                Debug.Log(error);
            });
        }
    }
    
    private void SyncChainPet(AssetListResponse response)
    {
        Dictionary<string, int> counter = new Dictionary<string, int>();
        foreach (Asset asset in response.Results)
        {
            string pet = PetInfo.NameToPetString(asset.Name);
            if (pet != null)
            {
                if (User.Instance.havePet.ContainsKey(pet))
                {
                    User.Instance.havePet[pet].onChain = true;
                }
                else
                {
                    User.Instance.havePet.Add(pet, new UserPet(pet, true));
                }
                
                if (!counter.ContainsKey(pet))
                {
                    counter.Add(pet, 0);
                }

                counter[pet]++;
            }
        }

        Dictionary<string, UserPet> pets = User.Instance.havePet;
        foreach (string key in pets.Keys)
        {
            if (!counter.ContainsKey(key))
            {
                pets[key].onChain = false;
            }
        }
    }
}
