﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PetGenerateMng : MonoBehaviour
{
    private readonly string playPetPath = "Prefabs/PlayPet/";

    public PlayMng playMng;

    public Slider hpSlider;
    public Text scoreText;
    public Text attackText;

    void Start()
    {
        Transform t = Instantiate(Resources.Load<Transform>($"{playPetPath}{User.Instance.cur.pet}"));
        t.SetParent(transform);
        t.localPosition = new Vector2(0, 0);

        PetMng p = t.GetComponent<PetMng>();

        p.getSound = GetComponent<AudioSource>();
        p.attackSound = transform.Find("AttackSound").GetComponent<AudioSource>();
        p.playMng = playMng;
        p.hpSlider = hpSlider;
        p.scoreText = scoreText;

        playMng.petMng = p;
    }
}
