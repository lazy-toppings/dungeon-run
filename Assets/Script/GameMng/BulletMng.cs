﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMng : MonoBehaviour
{
    private readonly string bulletPath = "Prefabs/Bullet/";

    public PlayMng playMng;

    private List<Transform> bullet = new List<Transform>();
    public Queue<int> available = new Queue<int>();
    
    private int cnt = 0;

    private Bullet bulletInfo;
    private int bulletLevel;

    public void Start()
    {
        bulletInfo = BulletInfo.Instance.bullets[$"{User.Instance.cur.pet}_{User.Instance.cur.bullet}"];
        bulletLevel = User.Instance.haveBullet[$"{User.Instance.cur.pet}_{User.Instance.cur.bullet}"].level;
    }

    public void Shoot(float y, float distanceMul = 1)
    {
        if (available.Count == 0)
        {
            GameObject g = Instantiate(Resources.Load($"{bulletPath}{User.Instance.cur.pet}_{User.Instance.cur.bullet}")) as GameObject;
            g.transform.SetParent(transform);
            g.GetComponent<BulletCollider>().idx = cnt;
            g.GetComponent<BulletCollider>().bulletMng = this;
            bullet.Add(g.transform);
            available.Enqueue(cnt++);
        }

        int idx = available.Dequeue();
        
        bullet[idx].localPosition = new Vector2(0, 0);
        bullet[idx].position = new Vector2(bullet[idx].position.x, y);
        bullet[idx].gameObject.SetActive(true);
        bullet[idx].GetComponent<Movement>().MoveStart(new Vector2(bulletInfo.attackDistance[bulletLevel] * distanceMul, bullet[idx].localPosition.y), BulletInfo.Instance.speed * bulletInfo.attackDistance[bulletLevel] / 5, () =>
        {
            bullet[idx].gameObject.SetActive(false);
            available.Enqueue(idx);
        });
    }
}