﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class UserBullet
{
    public string type;
    public int level;
    public int cnt;
    
    public UserBullet(string type)
    {
        this.type = type;
        level = 0;
        cnt = 1;
    }
}
