﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[Serializable]
public class UserPet
{
    public string type;
    public int level;
    public int exp;
    public int powerMode;
    public bool local;
    public bool onChain;
    public bool pass;

    public PetWork work;

    [NonSerialized]
    private int totalWeighted = 0;

    public UserPet(string type, bool chain = false)
    {
        this.type = type;
        level = 0;
        exp = 0;
        powerMode = 0;
        local = !chain;
        onChain = chain;
        work = new PetWork();
    }

    public string BulletItemWeigthed()
    {
        int total = CalTotalWeight();
        int ran = UnityEngine.Random.Range(0, total);

        int sum = 0;
        foreach (KeyValuePair<string, int> item in PetInfo.Instance.pets[type].itemWeighted[level])
        {
            sum += item.Value;
            if (ran <= sum) return item.Key;
        }
        throw new Exception("impossible weight of item");
    }

    public int CalTotalWeight(bool force = false)
    {
        if (totalWeighted != 0 && !force) return totalWeighted;
        totalWeighted = 0;
        foreach (int w in PetInfo.Instance.pets[type].itemWeighted[level].Values) totalWeighted += w;
        return totalWeighted;
    }

    public bool Active()
    {
        return local || onChain;
    }
}

[Serializable]
public class PetWork
{
    public bool isWork;
    public bool isReward;
    public DateTime workTime;

    public PetWork()
    {
        isWork = false;
        isReward = false;
        workTime = new DateTime();
    }
}
