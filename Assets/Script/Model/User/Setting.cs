﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Setting
{
    public bool soundOn;
    public bool tutoOn;

    public Setting()
    {
        tutoOn = true;
        soundOn = true;
    }
}
