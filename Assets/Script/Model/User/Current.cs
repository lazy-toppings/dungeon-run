﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Current
{
    public string pet;
    public string bullet;
    public int map;

    public Current()
    {
        pet = "-1";
        bullet = "0";
        map = 0;
    }
}
