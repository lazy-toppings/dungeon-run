﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class UserRank
{
    public string pet;
    public int meter;

    public UserRank(string pet, int meter)
    {
        this.pet = pet;
        this.meter = meter;
    }
}