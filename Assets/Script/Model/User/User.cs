﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Newtonsoft.Json;

[Serializable]
public class User : Singleton<User>
{
    public string id;
    
    public int money;

    public Current cur;
    public Setting setting;

    public Dictionary<string, UserPet> havePet;
    public Dictionary<string, UserBullet> haveBullet;
    public List<string> haveMap;
    public List<UserRank> rank;

    [NonSerialized]
    public bool notFirst = false;

    public User()
    {
        Set(null);
    }

    public string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }

    public void Set(User saved)
    {
        if (saved == null)
        {
            id = null;
            setting = new Setting();
            cur = new Current();
            havePet = new Dictionary<string, UserPet>();
            haveBullet = new Dictionary<string, UserBullet>();
            haveMap = new List<string>();
            rank = new List<UserRank>();
            money = 1000;
        }
        else
        {
            id = saved.id;
            setting = saved.setting;
            cur = saved.cur;
            havePet = saved.havePet;
            haveBullet = saved.haveBullet;
            haveMap = saved.haveMap;
            rank = saved.rank;
            money = saved.money;
        }
    }

    public void AddRank(int meter)
    {
        rank.Add(new UserRank(cur.pet, meter));
        rank = rank.OrderByDescending(r => r.meter).ToList();
        if (rank.Count > 20) rank = rank.GetRange(0, 20);
    }

    public void Save()
    {
        SaveLoadMng.SaveData(this);
    }
}
