﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerMode : Singleton<PowerMode>
{
    public List<string> type;
    public List<int> cnt;

    public PowerMode()
    {
        type = new List<string> { "monster", "coin" };
        cnt = new List<int> { 10, 15, 20 };
    }
}
