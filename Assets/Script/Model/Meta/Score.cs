﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class Coin : Singleton<Coin>
{
    public List<int> score;

    public Coin()
    {
        score = new List<int> { 5, 10, 15 };
    }
}