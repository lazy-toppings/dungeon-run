﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class PlayItem : Singleton<PlayItem>
{
    public List<string> haveItem;

    public PlayItem()
    {
        haveItem = new List<string> { "B0", "B1", "B2", "H0", "H1", "H2" };
    }
}