﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HpItem : Singleton<HpItem>
{
    public List<int> giveHp;

    public HpItem()
    {
        giveHp = new List<int> { 20, 40, 60 };
    }
}
