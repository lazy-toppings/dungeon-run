﻿using UnityEngine;
using System.Collections;

public class Distance : Singleton<Distance>
{
    public float Bonus(float distance)
    {
        if (distance < 100) return 1;
        if (distance < 200) return 1.1f;
        if (distance < 400) return 1.2f;
        if (distance < 700) return 1.3f;
        if (distance < 1000) return 1.4f;
        return 1.5f;
    }
}
