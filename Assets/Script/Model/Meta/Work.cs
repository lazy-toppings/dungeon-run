﻿using UnityEngine;
using System.Collections;
using System;

public class Work :Singleton<Work>
{
    public TimeSpan workDistance;
    public int getExp;

    public Work()
    {
        workDistance = new TimeSpan(0, 0, 10);
        getExp = 50;
    }
}
