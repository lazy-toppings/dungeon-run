﻿using UnityEngine;
using UnityEditor;

public class Monster : Singleton<Monster>
{
    public float damage;
    public int exp;

    public Monster()
    {
        damage = 20;
        exp = 5;
    }
}