﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class Pet
{
    public List<float> powerModeTime;
    public List<float> powerModePower;
    public List<float> powerModeShootInterval;

    public List<float> hp;
    public List<float> hpDecrease;
    public List<float> hpInterval;

    public List<float> damageControl; // hp -= moster.damage * damageControl
    public List<float> coinControl; // user.money = coin.coin * coinControl
    public List<float> expControl; // user.exp = run_distance * expControl

    public int maxLevel;
    public List<int> levelUpExp;
    public List<int> levelUpCost;

    public List<Dictionary<string, int>> itemWeighted;
    public List<int> itemPr;

    public string origin;
    public string feature;
    public string name;
}