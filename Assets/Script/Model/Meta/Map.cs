﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map : Singleton<Map>
{
    public List<string> possible;
    public Dictionary<string, int> mapCost;

    public List<float> monsterPr;

    public List<float> speed;
    public List<int> speedDistance;
    public int maxLevel;
    public Dictionary<int, int> bgCnt;
    public Dictionary<int, int> blockCnt;
    
    public Map()
    {
        possible = new List<string> { "0", "1" };
        mapCost = new Dictionary<string, int> { { "0", 0 }, { "1", 10000 } };

        monsterPr = new List<float> { 3, 2.7f, 2.5f, 2, 1.7f, 1.5f, 1.5f };

        speed = new List<float> { 3, 3.5f, 4f, 4.5f, 5f, 5.5f, 6 };
        speedDistance = new List<int> { 15, 70, 130, 200, 400, 600 };
        maxLevel = 6;
        bgCnt = new Dictionary<int, int> { { 0, 1 }, { 1, 1 } };
        blockCnt = new Dictionary<int, int> { { 0, 6 }, { 1, 5 } };
    }
}
