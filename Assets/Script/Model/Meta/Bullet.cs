﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class Bullet
{
    public List<float> attackDistance;
    public List<int> maxCapacity;
    public List<float> regenTime;


    public int maxLevel;
    public List<int> levelUpCnt;
    public List<int> levelUpCost;
}