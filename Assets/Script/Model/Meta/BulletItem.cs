﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletItem : Singleton<BulletItem>
{
    public List<int> giveCnt;

    public BulletItem()
    {
        giveCnt = new List<int> { 2, 4, 6 };
    }
}
