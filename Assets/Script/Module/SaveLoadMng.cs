﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class SaveLoadMng
{
    public static string dataDir = "/data.save";

    public static void SaveData(User data) { Save(data, dataDir); }

    public static User LoadData()
    {
        User data = Load<User>(dataDir);

        return data;
    }

    private static void Save(object objSaveLoad, string saveDir)
    {
        if (objSaveLoad == null)
            return;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + saveDir, FileMode.Create);

        bf.Serialize(stream, objSaveLoad);

        stream.Close();
    }

    private static T Load<T>(string saveDir)
    {
        if (!File.Exists(Application.persistentDataPath + saveDir))
            return default(T);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + saveDir, FileMode.Open);

        T objSaveLoad = default(T);

        try
        {
            objSaveLoad = (T)bf.Deserialize(stream);
        }
        catch (SerializationException e)
        {
            Debug.Log("failed to deserialize. Reason : " + e.Message);

            stream.Close();
            File.Delete(Application.persistentDataPath + saveDir);
        }
        finally
        {
            stream.Close();
        }

        return objSaveLoad;
    }
}