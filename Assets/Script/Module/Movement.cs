﻿using UnityEngine;
using System.Collections;
using System;

public class Movement : MonoBehaviour
{
    private float timer;
    private float speed;
    private bool isPause;
    private Action afterDone;
    private Vector2 now;
    private Vector2 move = new Vector2(0, 0);

    private Coroutine moveRoutine;

    public void MoveStart(Vector2 move, float time, Action afterDone, bool timeExpand = false)
    {
        if (moveRoutine != null) StopCoroutine(moveRoutine);

        now = transform.localPosition;

        if (timeExpand) time = time * Vector2.Distance(move, now) / PetInfo.Instance.jumpHeight * 1.5f;
        speed = Vector2.Distance(move, now) / time * Time.deltaTime;
        this.move = move;
        this.afterDone = afterDone;

        timer = 0;
        moveRoutine = StartCoroutine(MoveRoutine());
    }

    IEnumerator MoveRoutine()
    {
        while (timer <= 1f)
        {
            if (!isPause)
            {
                timer += speed;
                transform.localPosition = Vector3.Lerp(now, move, timer);
            }
            yield return new WaitForSeconds(0);
        }
        afterDone?.Invoke();
    }

    public void Pause(bool isPause)
    {
        this.isPause = isPause;
    }

    public void Stop()
    {
        isPause = false;
        StopAllCoroutines();
    }
}
