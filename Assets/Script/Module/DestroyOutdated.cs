﻿using UnityEngine;
using System.Collections;

public class DestroyOutdated : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(DestroyRoutine());
    }
    
    IEnumerator DestroyRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            
            foreach (Transform t in transform)
            {
                if (t.GetComponent<Renderer>().isVisible) break;
                Destroy(t.gameObject);
            }
        }
    }
}
